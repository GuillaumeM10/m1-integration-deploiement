<?php
$bdd_params_object = new stdClass;
$bdd_params_object->db_host = getenv('RDS_HOSTNAME');
$bdd_params_object->db_name = getenv('RDS_DB_NAME');
$bdd_params_object->db_port = getenv('RDS_PORT') ?: 3306;
$bdd_params_object->user = getenv('RDS_USERNAME');
$bdd_params_object->password = getenv('RDS_PASSWORD');
$bdd_params_object->connexion = 'host=';
$bdd_params_object->charset = 'utf8';

try {
    $bdd = new PDO('mysql:' . $bdd_params_object->connexion . $bdd_params_object->db_host . ';dbname=' . $bdd_params_object->db_name . ';charset=' . $bdd_params_object->charset, $bdd_params_object->user, $bdd_params_object->password);
} catch (Exception $e) {
    echo "ERREUR CONNEXION BDD";
    echo '<pre>';
    print_r($bdd_params_object);
    echo '</pre>';
    die('Erreur : ' . $e->getMessage());
}

$req_insert = $bdd->prepare('INSERT INTO city (name, country, population) VALUES (:name, :country, :population)');

$name = 'Nouvelle Ville';
$country = 'Nouveau Pays';
$population = 1000000;
$req_insert->bindParam(':name', $name);
$req_insert->bindParam(':country', $country);
$req_insert->bindParam(':population', $population);
$req_insert->execute();

$req_select = $bdd->prepare('SELECT * FROM city');
$req_select->execute();
var_dump($req_select->fetchAll());
