# Usefull commands

```
# Login to gitlab registry
docker login registry.gitlab.com


# Build the image from gitlab registry
docker run -d -p 8090:80 --name ynov registry.gitlab.com/guillaumem10/m1-integration-deploiement:main
## http://localhost:8090/

# Exectute commandes in image
docker exec -it ynov /bin/bash

# Stop the container
docker stop ynov

# Remove the container
docker rm ynov

# Remove the image
docker rmi registry.gitlab.com/guillaumem10/m1-integration-deploiement:main

# Push the image to gitlab registry
docker push registry.gitlab.com/guillaumem10/m1-integration-deploiement:main

# Build the image from dockerfile
docker build -t registry.gitlab.com/guillaumem10/m1-integration-deploiement:main .

# Remove from repository
git rm -r src/vendor --cached

# tests with robotframework
python -m robot robotframework/

```

## Infos

- Packages & registries > Container Registry
